@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        <form action = '/palpite' method = 'POST'>
        {{csrf_field()}}    
            <div class="container">
                <h2>Palpites</h2>
                <p>Dê seu palpite:</p>            
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Seleção</th>
                            <th>Placar</th>
                            <th>Placar</th>
                            <th>Seleção</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($jogos as $jogo)
                            <tr> 
                                <td>{{$jogo['jogo_selecao1_nome']}}<input type = 'hidden' name = 'jogo_selecao1_nome_{{$jogo['id']}}' value = '{{$jogo['jogo_selecao1_nome']}}'></td>
                                @if($jogo['jogo_estado'] == 'FINISHED')
                                    <td>{{$jogo['jogo_placar_selecao1']}}</td>
                                    <td>{{$jogo['jogo_placar_selecao2']}}</td>
                                    @else
                                        <td><input type = 'number' name = 'palpite_placar_selecao1_{{$jogo['id']}}'></td>
                                        <td><input type = 'number' name = 'palpite_placar_selecao2_{{$jogo['id']}}'></td>
                                @endif
                                <td>{{$jogo['jogo_selecao2_nome']}}<input type = 'hidden' name = 'jogo_selecao2_nome_{{$jogo['id']}}' value = '{{$jogo['jogo_selecao2_nome']}}'></td>
                                @if($jogo['jogo_estado'] != 'FINISHED')
                                    
                                @endif  
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <button type="submit" class="btn btn-primary">{{ __('Confirmar') }}</button>
            </div>
        </form>
    </div>
</div>
@endsection
