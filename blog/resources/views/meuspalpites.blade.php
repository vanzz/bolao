@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">  
            <div class="container">
                <h2>Palpites</h2>
                <p>Seus palpite:</p>            
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Seleção</th>
                            <th>Placar</th>
                            <th>Placar</th>
                            <th>Seleção</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($palpites as $palpite)
                            <tr> 
                                <td>{{$palpite['jogo_selecao1_nome']}}</td>
                                <td>{{$palpite['jogo_placar_selecao1']}}</td>
                                <td>{{$palpite['jogo_placar_selecao2']}}</td></td>
                                <td>{{$palpite['jogo_selecao2_nome']}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
