<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJogosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('jogos')){
            Schema::create('jogos', function (Blueprint $table) {
                $table->increments('id');
                $table->string('jogo_selecao1_nome');
                $table->string('jogo_selecao2_nome');
                $table->integer('jogo_placar_selecao1')->unsigned()->nullable();
                $table->integer('jogo_placar_selecao2')->unsigned()->nullable();;
                $table->string('jogo_datahora');
                $table->string('jogo_estado');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jogos');
    }
}
