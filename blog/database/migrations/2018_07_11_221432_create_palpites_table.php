<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePalpitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('palpites')){
            Schema::create('palpites', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id');
                $table->string('jogo_selecao1_nome');
                $table->string('jogo_selecao2_nome');
                $table->integer('palpite_placar_selecao1')->unsigned();
                $table->integer('palpite_placar_selecao2')->unsigned();
                $table->integer('palpite_pontos')->unsigned()->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('palpites');
    }
}
