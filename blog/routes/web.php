<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/palpites', 'PalpiteController@index')->name('palpites');
Route::get('/jogos', 'JogoController@index')->name('jogos');
Route::post('/palpite', 'PalpiteController@palpite')->name('palpite');
Route::get('/meuspalpites', 'PalpiteController@listar')->name('meuspalpites');