<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jogo extends Model
{

    protected $table = 'jogos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'jogo_selecao1_nome', 'jogo_selecao2_nome', 'jogo_placar_selecao1', 'jogo_placar_selecao2', 'jogo_datahora', 'jogo_estado'
    ];
}
