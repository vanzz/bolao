<?php

namespace App\Http\Controllers;

use App\Jogo;
use Illuminate\Http\Request;

class JogoController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$this->plmdds();
        return view('jogos');
    }

    public function plmdds()
    {
        $uri = 'http://api.football-data.org/v1/competitions/467/fixtures';
        $reqPrefs['http']['method'] = 'GET';
        $reqPrefs['http']['header'] = array(env('API_FOOTBALL_KEY'));
        $stream_context = stream_context_create($reqPrefs);
        $response = file_get_contents($uri, false, $stream_context);
        $jogos = json_decode($response, true);

        foreach($jogos['fixtures'] as $jogo){
            Jogo::create([
                'jogo_selecao1_nome' => $jogo['homeTeamName'], 
                'jogo_selecao2_nome' => $jogo['awayTeamName'], 
                'jogo_placar_selecao1' => $jogo['result']['goalsHomeTeam'], 
                'jogo_placar_selecao2' => $jogo['result']['goalsAwayTeam'], 
                'jogo_datahora' => $jogo['date'],
                'jogo_estado' => $jogo['status']
            ]);
        }

        return view('jogos', compact('jogos'));
    }
}
