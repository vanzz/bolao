<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Jogo;
use App\Palpite;
use Illuminate\Http\Request;

class PalpiteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jogos = Jogo::get();
        return view('palpites', compact('jogos'));
    }

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/palpites';

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     
        * protected function validator(array $data)
        * {
        *     return Validator::make($data, [
        *         'jogo_selecao1_nome' => 'required|string|max:255',
        *         'jogo_selecao2_nome' => 'required|string|max:255',
        *         'palpite_placar_selecao1' => 'required|integer|max:255',
        *         'palpite_placar_selecao2' => 'required|integer|max:255',
        *     ]);
        * }
    */

    public function palpite(){
        $jogos = Jogo::get();

        foreach($jogos as $jogo){
            $jogo_selecao1_nome = 'jogo_selecao1_nome_'.$jogo["id"];
            $jogo_selecao2_nome = 'jogo_selecao2_nome_'.$jogo["id"];
            $palpite_placar_selecao1 = 'palpite_placar_selecao1_'.$jogo["id"];
            $palpite_placar_selecao2 = 'palpite_placar_selecao2_'.$jogo["id"];
            
            if(!empty(request($palpite_placar_selecao1))){
                Palpite::create([
                    'user_id' => Auth::id(),
                    'jogo_selecao1_nome' => request($jogo_selecao1_nome),
                    'jogo_selecao2_nome' => request($jogo_selecao2_nome),
                    'palpite_placar_selecao1' => request($palpite_placar_selecao1),
                    'palpite_placar_selecao2' => request($palpite_placar_selecao2),
                ]);
            }
        }
        return redirect('/palpites');
    }

    public function listar(){
        $palpites = Palpite::get();
        return redirect('/meuspalpites');
    }
}
