<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Palpite extends Model
{

    protected $table = 'palpites';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'jogo_selecao1_nome', 'jogo_selecao2_nome', 'palpite_placar_selecao1', 'palpite_placar_selecao2', 'palpite_pontos',
    ];
}
